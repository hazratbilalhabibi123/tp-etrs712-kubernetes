# TP-ETRS712-kubernetes


pour utiliser cette projet premier clonner cette projet dans votre dossier local et puis utilié la commande apply pour demarrer tous les pods.
on a utilisé le port 32123 pour access à cette service depuis l'exterieur.

## Clonner en utilisent protocole HTTP

```
git clone git@gitlab.com:hazratbilalhabibi123/tp-etrs712-kubernetes.git
```


## Clonner en utilisent protocole SSH

```
git clone https://gitlab.com/hazratbilalhabibi123/tp-etrs712-kubernetes.git
```


## Deployer tous le projet
on peut éployer toute l'application en une seule fois en utilisant la commande `kubectl apply`

```
kubectl apply -f k8s/
```

## Commande utiles pour kubernetes
```
microk8s.kubectl apply -f < DIRECTORY NAME >
microk8s.kubectl delete -f < DIRECTORY NAME >
microk8s.kubectl get pods
microk8s.kubectl get pv
microk8s.kubectl get pvc
microk8s.kubectl get deploy
microk8s.kubectl describe pvc < PVC NAME >
microk8s.kubectl describe pod < POD NAME >
microk8s.kubectl describe deploy < DEPLOYMENT NAME >
microk8s.kubectl describe exec -it < POD NAME > bash

microk8s.kubectl create configmap configmap --from-file='dump/app.sql' --dry-run=client -o yaml

microk8s.kubectl get replicasets
microk8s.kubectl delete replicasets mysql-68bc4c6f4b
```
